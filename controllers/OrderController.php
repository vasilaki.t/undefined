<?php


namespace app\controllers;


use app\models\Order;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

/**
 * Class OrderController
 * @package app\Controllers
 */
class OrderController extends ActiveController
{
    public $modelClass = 'app\models\Order';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * @return array
     */
    public function actions()
    {
        $action = parent::actions();
        unset($action['update']);
        unset($action['delete']);
        return $action;
    }

    /**
     * @param $id
     * @return Order|null
     */
    public function actionPay($id)
    {
        $order = Order::findOne($id);
        $order->status = $order::STATUS_PAYED;
        $order->save();
        return $order;
    }

    /**
     * @return ActiveDataProvider
     */
    public function actionList()
    {
        return $activeData = new ActiveDataProvider([
            'query' => Order::find(),
        ]);
    }
}