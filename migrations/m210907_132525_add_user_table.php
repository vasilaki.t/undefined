<?php

use yii\db\Migration;

/**
 * Class m210907_132525_add_user_table
 */
class m210907_132525_add_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tbl_user', [
            'id' => $this->primaryKey(),
            'login' => $this->string(50),
            'password' => $this->string(50),
            'access_token' => $this->string(50),
            'last_login' => $this->dateTime(),

        ]);
        $this->createIndex('i_login', 'tbl_user', 'login');
        $this->createIndex('i_token', 'tbl_user', 'access_token');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('i_login', 'tbl_user');
//        $this->dropIndex('i_token', 'tbl_user');
        $this->dropTable('tbl_user');
        return true;
    }


}
