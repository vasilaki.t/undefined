<?php

use app\models\CarManufacturer;
use app\models\CarModel;
use app\models\ProductPrices;
use app\models\ProductRemains;
use app\models\Products;
use yii\db\Migration;

/**
 *  Add cars data
 * Class m210906_123516_add_cars_data
 */
class m210906_123516_add_cars_data extends Migration
{

    private $products = [
        'Дверь',
        'Бампер',
        'Крыло',
        'Колесо',
        'Фильтр',
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $data = file_get_contents(__DIR__ .'../start/cars.txt');
        $carRows = explode("\n", $data);
        $carList = $productsList = [];
        foreach ($carRows as $row) {
            $tmp = explode(',', $row);
            $carList[$tmp[0]]['models'][] = [
                'name' => $row, 'id' => 0
            ];
        }

        foreach (array_keys($carList) as $carManufacturer) {
            if ($carManufacturer) {
                $manufacturer = new CarManufacturer();
                $manufacturer->name = $carManufacturer;
                if ($manufacturer->save()) {
                    $carList[$carManufacturer]['id'] = $manufacturer->id;
                }
            }

        }

        foreach ($this->products as $product) {
            $model = new Products();
            $model->name = $product;
            $model->save();
            $productsList[] = $model->id;
        }

        foreach ($carList as $carManufacturer => $carModels) {
            if ($carModels['id']) {
                foreach ($carModels['models'] as $key => $carModel) {
                    if ($carModel['name']) {
                        $model = new CarModel();
                        $model->manufacturer_id = $carModels['id'];
                        $model->name = $carModel['name'];
                        if ($model->save()) {
                            $carList[$carManufacturer][$key]['id'] = $model->id;
                        }

                        foreach ($productsList as $product) {
                            $price = new ProductPrices();
                            $price->model_id = $model->id;
                            $price->product_id = $product;
                            $price->price = rand(100, 1000);
                            $price->save();

                            $remain = new ProductRemains();
                            $remain->price_id = $price->id;
                            $remain->quantity = rand(0, 100);
                            $remain->reserve = $remain->quantity > 0 ? round($remain->quantity / 2) : 0;
                            $remain->save();
                        }

                    }
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('tbl_car_manufacturer');
        $this->truncateTable('tbl_car_model');
        $this->truncateTable('tbl_product_remains');
        $this->truncateTable('tbl_product_prices');
    }
}
