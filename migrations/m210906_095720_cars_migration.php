<?php

use yii\db\Migration;

/**
 *
 * Add cars migration
 * Class m210906_095720_cars_migration
 */
class m210906_095720_cars_migration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tbl_car_manufacturer', [
            'id' => $this->primaryKey(),
            'name' => $this->string(20),
            //TODO not null uin
        ]);


        $this->createTable('tbl_car_model', [
            'id' => $this->primaryKey(),
            'manufacturer_id' => $this->integer(),
            'name' => $this->string(),
        ]);

        $this->createIndex('i_manufacturer_id', 'tbl_car_model', 'manufacturer_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('i_manufacturer_id', 'tbl_car_model');

        $this->dropTable('tbl_car_manufacturer');
        $this->dropTable('tbl_car_model');
        return true;
    }

}
