<?php

use yii\db\Migration;

/**
 * Add product table migration
 * Class m210906_100418_product
 */
class m210906_100418_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tbl_products', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),

        ]);

        $this->createTable('tbl_product_prices', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'model_id' => $this->integer(),
            'price' => $this->float(),
        ]);

        $this->createIndex('i_model_id', 'tbl_product_prices', 'model_id');

        $this->createTable('tbl_product_remains', [
            'id' => $this->primaryKey(),
            'price_id' => $this->integer(),
            'quantity' => $this->integer(),
            'reserve' => $this->integer(),
        ]);

        $this->createIndex('i_price_id', 'tbl_product_remains', 'price_id');



    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->dropIndex('i_model_id', 'tbl_product_prices');
//        $this->dropIndex('i_price_id', 'tbl_product_remains');
        
        $this->dropTable('tbl_products');
        $this->dropTable('tbl_product_prices');
        $this->dropTable('tbl_product_remains');

        return true;
    }

}
