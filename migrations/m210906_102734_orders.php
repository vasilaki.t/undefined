<?php

use yii\db\Migration;

/**
 * add orders and basket tables
 * Class m210906_102734_orders
 */
class m210906_102734_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tbl_orders', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'create_at' => $this->dateTime(),
            'status' => $this->string(),

        ]);


        $this->createTable('tbl_basket', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'product_id' => $this->integer(),
            'quantity' => $this->integer(),
            'price'  => $this->float(),
        ]);

        $this->createIndex('i_user_id', 'tbl_orders', 'user_id');
        $this->createIndex('i_status', 'tbl_orders', 'status');
        $this->createIndex('i_create', 'tbl_orders', 'create_at');

        $this->createIndex('i_order_id', 'tbl_basket', 'order_id');
        $this->createIndex('i_product_id', 'tbl_basket', 'product_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('i_user_id', 'tbl_orders');
        $this->dropIndex('i_order_id', 'tbl_basket');
        $this->dropIndex('i_product_id', 'tbl_basket');
        $this->dropIndex('i_status', 'tbl_orders');
        $this->dropIndex('i_create', 'tbl_orders');

        $this->dropTable('tbl_orders');
        $this->dropTable('tbl_basket');


        return true;
    }

}
