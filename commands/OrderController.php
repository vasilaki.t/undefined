<?php

namespace app\commands;

use app\models\Order;
use app\models\ProductRemains;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Class OrderCommand
 * @package app\commands
 */
class OrderController extends Controller
{
    /**
     * cancel order
     */
    public function actionIndex()
    {
        try {
            $orders = Order::find()
                ->where(['status' => Order::STATUS_NEW,])
                ->andWhere(['>=', 'create_at', 'DATE_ADD(NOW(), INTERVAL -2 HOUR)'])
                ->with(['goods'])
                ->all();

            foreach ($orders as $order) {
                $order->status = $order::STATUS_CANCEL;
                $order->save();
                foreach ($order->goods as $good) {
                    $remain = ProductRemains::find(['price_id' => $good->product_id]);
                    $remain->reserve -= $good->quantity;
                    $remain->save();
                }
            }

        } catch (\Exception $exception) {
            return ExitCode::UNSPECIFIED_ERROR;
        }

        return ExitCode::OK;
    }
}