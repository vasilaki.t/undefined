<?php

namespace app\helpers;

use app\models\BasketForm;
use Yii;
use yii\helpers\Json;

/**
 * Class OrderMakerHelper
 * @package app\helpers
 */
class OrderMakerHelper
{
    /**
     * @param \app\models\Order $order
     * @return BasketForm
     */
    public static function getOrderRequest(\app\models\Order $order)
    {
        $requestBody = Json::decode(Yii::$app->request->getRawBody());

        $form = new BasketForm();

        if ($requestBody) {
            $form->attributes = $requestBody;
            $form->setOrder($order);
        }

        return $form;
    }
}