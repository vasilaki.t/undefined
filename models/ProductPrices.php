<?php

namespace app\models;

/**
 * This is the model class for table "{{%tbl_product_prices}}".
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $model_id
 * @property float|null $price
 */
class ProductPrices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_product_prices}}';
    }

    public function fields()
    {
        return [
            'id',
            'price',
            'car' => function ($model) {
                return $model->car->manufacturer->name . ' ' . $model->car->name;
            },
            'product' => function ($model) {
                return $model->product->name;
            },
            'remain' => function ($model) {
                return $model->remains;
            },


        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'model_id'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @return mixed
     */
    public function getCar()
    {
        return $this->hasOne(CarModel::class, ['id' => 'model_id']);
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->hasOne(Products::class, ['id' => 'product_id']);
    }

    /**
     * @return mixed
     */
    public function getRemains()
    {
        return $this->hasOne(ProductRemains::class, ['price_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'model_id' => 'Model ID',
            'price' => 'Price',
        ];
    }

    /**
     * @return integer
     */
    public function getRemainQuantity()
    {
        return $this->remains->quantity - $this->remains->reserve;
    }

}
