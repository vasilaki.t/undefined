<?php

namespace app\models;

use app\helpers\OrderMakerHelper;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tbl_orders}}".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $create_at
 * @property string status
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    private $goods = [];

    const STATUS_NEW = 'new';
    const STATUS_CANCEL = 'cancel';
    const STATUS_PAYED = 'payed';
    const STATUS_IMPOSSIBLE = 'impossible';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_orders}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['create_at', 'status'], 'safe'],
        ];
    }

    /**
     * good relation
     * @return mixed
     */
    public function getGoods()
    {
        return $this->hasMany(Basket::class, ['order_id' => 'id']);
    }

    /**
     * user relation
     * @return mixed
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $oldOrder = self::findOne($this->id);

        if ($oldOrder->status !== $this->status && !empty($this->user)) {
            Yii::$app->mailer->compose()
                ->setTo($this->user->login)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setSubject(sprintf('Order #%d status change %s', $this->id, $this->status))
                ->setTextBody(sprintf('Order #%d status change %s', $this->id, $this->status))
                ->send();
        }

        if ($this->isNewRecord) {
            $orderForm = OrderMakerHelper::getOrderRequest($this);
            //$orderForm->user = 1; todo get  User


            if ($orderForm->validate()) {
                $this->user_id = 1;
                $this->create_at = new \yii\db\Expression('NOW()');
                $this->status = self::STATUS_NEW;
                return parent::beforeSave($insert);
            } else {

                $this->addError('goods', $orderForm->getErrors());
                return false;
            }
        } else {
            return parent::beforeSave($insert);
        }

    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        if ($this->status == self::STATUS_NEW) {
            foreach ($this->goods as $good) {

                $good->order_id = $this->id;

                $productRemains = ProductRemains::find(['price_id' => $good->product_id])->one();
                $availGoods = $productRemains->quantity - $productRemains->reserve;

                if ($availGoods >= $good->quantity) {
                    $productRemains->reserve += $good->quantity;
                    $productRemains->save();
                } else {
                    $this->status = self::STATUS_IMPOSSIBLE;
                    $this->save();
                }
                $good->save();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param $goods
     */
    public function setGoods($goods)
    {
        if (is_array($goods)) {
            ArrayHelper::merge($this->goods, $goods);
        } else {
            $this->goods[] = $goods;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'create_at' => 'Create At',
        ];
    }
}
