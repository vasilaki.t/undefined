<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_car_model}}".
 *
 * @property int $id
 * @property int|null $manufacturer_id
 * @property string|null $name
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_car_model}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manufacturer_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacturer_id' => 'Manufacturer ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return mixed
     */
    public function getManufacturer()
    {
        return $this->hasOne(CarManufacturer::class, ['id' => 'manufacturer_id']);
    }
}
