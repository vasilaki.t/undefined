<?php

namespace app\models;

use app\models\Basket;
use app\models\Order;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class BasketForm
 * @package app\models
 */
class BasketForm extends Model
{
    /**
     * @var array
     */
    public $goods;
    /**
     * @var string
     */
    public $user;
    /**
     * @var \app\models\Order
     */
    private $order;

    public function setOrder(\app\models\Order $order)
    {
        $this->order = $order;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['goods', 'user'], 'required'],
        ];
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     * @return bool|void
     */
    public function afterValidate($attributeNames = null, $clearErrors = true)
    {
        $goods = $this->goods;

        $priceRemain = ArrayHelper::map(ProductPrices::find()
            ->where(['in', 'id', array_keys($goods)])
            ->with('remains')
            ->all(), 'id', function ($row) {
            return $row;
        });

        foreach ($goods as $key => $goodQuantity) {
            if (key_exists($key, $priceRemain)) {
                if ($priceRemain[$key]->getRemainQuantity() > $goodQuantity) {
                    $orderItem = new Basket();
                    $orderItem->product_id = $key;
                    $orderItem->quantity = $goodQuantity;
                    $orderItem->price = $priceRemain[$key]->price;

                    if ($orderItem->validate()) {
                        $this->order->setGoods($orderItem);
                    } else {
                        $this->addError('goods', $orderItem->getError());
                    }
                } else {
                    $this->addError('goods', sprintf('Нет возможности поставить резерв товар #%d', $key));
                }

            } else {
                $this->addError('goods', sprintf('Товар не найден #%d', $key));
            }
        }

        return $this->hasErrors() === false;
    }
}