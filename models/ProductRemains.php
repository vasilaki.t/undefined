<?php

namespace app\models;

/**
 * This is the model class for table "{{%tbl_product_remains}}".
 *
 * @property int $id
 * @property int|null $price_id
 * @property int|null $quantity
 * @property int|null $reserve
 */
class ProductRemains extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_product_remains}}';
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'quantity',
            'reserve'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_id', 'quantity', 'reserve'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_id' => 'Price ID',
            'quantity' => 'Quantity',
            'reserve' => 'Reserve',
        ];
    }
}
